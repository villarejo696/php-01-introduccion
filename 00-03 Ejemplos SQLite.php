<?php
//variables necesarias
$crear_tabla = 'CREATE TABLE IF NOT EXISTS empleado (
			id INTEGER PRIMARY KEY, 
			nombre TEXT,
			email TEXT
			) ';
$empleados = array(
			array('nombre' => "Luis Miguel",
				  'email'=> "luismiguel@daw.es"
				  ),
			array('nombre' => "Pilar",
				  'email'=> "pilarl@daw.es"
				  )		  
		);
?>
<?php
//test de accesp a sqlite

try{
	// 1.- Crear base de datos (sqlite solo la ruta, mysql, ip, usuario y password)
	$conn = new PDO('sqlite:empleados.db'); //crea conexion y crea la bd si no existe
	
		//crear base de datos en memoria.
		//		$conn = new PDO('sqlite::memory:');
		
	$conn -> exec($crear_tabla); //crear tabla
	
	// 2.- Preparamos la sentencia de insercion
	$insertar = "INSERT INTO empleado (nombre, email) 
				VALUES (:nombre, :email)";
				
	$sentencia  = $conn -> prepare($insertar);
	foreach($empleados as $empleado){ //recorremos el arrat de empleados
		$sentencia -> execute($empleado); //ejecutamos la insercion con los datos de un empleado
		echo "Insertado ", $empleado['nombre'], "<br />"; //mostramos el nombre del empleado insertado
	}
	
	// 3.- Seleccionamos datos de la bd
	$sentencia = "SELECT * FROM empleado"; //Preparamos la sentencia
	$resultado = $conn -> query($sentencia); //Hacemos la consulta con la conexion a la bd
	foreach ($resultado as $empleado) {
		echo "<strong>Empleado:</strong> ", $empleado['nombre'], " (", $empleado['email'], ")<br/ >";		
	}
	
}catch(PDOException $e ){
	echo $e->getMessage();
	
}
/*
 * LOG DE ERRORES: tail -f /var/log/apache2/error.log
 * 		POSIBLES ERRORES     ------> 	SOLUCIONES
 * could not find driver ------  1. apt-cache search php5 sqlite 
 * 							     2. sudo apt-get install php5-sqlite
 * 							     3. sudo gedit /etc/php5/apache2/php.ini
 * 								 4. buscar sqlite: sqlite=sqlite3.so al final
 * 								 5. sudo service apache2 reload
 
 * * Unable acces database       1. Apache entra como usuario otros: No tiene permisos
 * 								 2. Crear archivo a mano y darle permiso de escritura al arhivo
 * 									o dar permiso de escritura en el directorio para otros
 * 									chmod o+w  
 * 
 */
?>

