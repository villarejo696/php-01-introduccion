<!DOCTYPE html>
<html>
	<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
  		<title>Calculadora de divisiones (Formulario). if ... elseif ... else ... Ejercicios. PHP. Bartolomé Sintes Marco</title>
	</head>
	<body>
		<h1>Calculadora de divisiones (Formulario)</h1>
		<form action="02-01 Calculadora Divisiones.php" method="get">
			  <fieldset>
				    <legend>Formulario</legend>
				    <p>Escriba el dividendo y el divisor (0 &le; dividendo &lt; 1.000; 0 &lt; divisor &lt; 1.000) para calcular el cociente y el resto de la división.</p>				
				    <table cellspacing="5" class="borde">
					      <tbody>
						        <tr>
							          <td>
							          		<strong>Dividendo:</strong>
							          </td>
							          <td>
							          		<input type="text" name="dividendo" size="5" maxlength="5" />
							          </td>
						        </tr>
						        <tr>
							          <td>
							          		<strong>Divisor:</strong>
							          </td>
							          <td>
							          		<input type="text" name="divisor" size="5" maxlength="5" />
							          </td>
						        </tr>
					      </tbody>
				    </table>				
				    <p class="der">
				    	<input type="submit" value="Calcular" /> 
				    	<input type="reset" value="Borrar" name="Reset" />
				    </p>
			  </fieldset>
		</form>
		<?php
			if (isset($_GET['dividendo'], $_GET['divisor'])) {
				$dividendo = $_GET['dividendo'] ;
				$divisor = $_GET['divisor'] ;
				$resultado = 0;
				
				if(is_numeric($dividendo) && is_numeric($divisor)){
					if ($dividendo >= 0 && $dividendo <= 1000 && $divisor > 0 && $divisor < 1000) {
						$resultado = $dividendo / $divisor;
						printf ("<p>El resultado de $dividendo / $divisor = %.2f</p>", $resultado);	
					} else {
						printf ("<p>Debe introducir los datos correctos	(0 <= dividendo < 1.000; 0 < divisor < 1.000)</p>");
					}
				}else{
						printf ("<p>Debe introducir un numero</p>");			
				}					
			}
		?>
	</body>
</html>