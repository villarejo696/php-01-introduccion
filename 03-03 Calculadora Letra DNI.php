<!DOCTYPE html>
<html>
	<head>
		  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		  <title>Calculadora de letra del DNI (Formulario). Repaso 1. Ejercicios. PHP. Bartolomé Sintes Marco</title>
	</head>	
	<body>
		<h1>Calculadora de letra del DNI (Formulario)</h1>
		<form action="03-03 Calculadora Letra DNI.php" method="get">
			  <fieldset>
				    <legend>Formulario</legend>
				    <p>Escriba el número del DNI y calcularé la letra correspondiente.</p>
				    <table cellspacing="5" class="borde">
					      <tbody>
						        <tr>
							          <td>
							          		<strong>Número:</strong>
							          </td>
							          <td>
							          		<input type="text" name="dni" size="10" maxlength="10" />
							          </td>
						        </tr>
					      </tbody>
				    </table>
			    	<p class="der">
			    		<input type="submit" value="Calcular" /> 
			    		<input type="reset" value="Borrar" name="Reset" />
			  		</p>
			  </fieldset>
		</form>
		<?php
			if (isset($_GET['dni'])) {
				$dni = $_GET['dni'];	
				$letrasDNI = 'TRWAGMYFPDXBNJZSQVHLCKE';	
				if(is_numeric($dni)){
					$num = $dni % 23;
					printf ("<p>El dni completo es: " . $dni . "-" . $letrasDNI[$num] ."</p>");
				}else{
					printf ("<p>Debe introducir un numero</p>");			
				}		
			}
		?>
	</body>
</html>