<!DOCTYPE html>
<html>
	<head>		
		  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		  <title>Convertidor de bytes (Formulario). Repaso 1. Ejercicios. PHP. Bartolomé Sintes Marco</title>
	</head>	
	<body>
		<h1>Convertidor de bytes (Formulario)</h1>
		<form action="03-02 Conversor De Bytes.php" method="get">
			<fieldset>
				<legend>Formulario</legend>
				<p>Escriba un número de bytes (0 &le; bytes &lt; 2.000.000.000) y lo convertiré a KB, MB y GB.</p>
				<table cellspacing="5" class="borde">
					<tbody>
						<tr>
							<td
								<strong>Bytes:</strong>
							</td>
							<td>
								<input type="text" name="bytes" size="10" maxlength="10" />
							</td>
						</tr>
					</tbody>
				</table>
				<p class="der">
					<input type="submit" value="Convertir" /> 
					<input type="reset" value="Borrar" name="Reset" />
				</p>
			</fieldset>
		</form>
		<?php
			if (isset($_GET['bytes'])) {
				$bytes = $_GET['bytes'];
				if(is_numeric($bytes)){
					if ($bytes >= 0 && $bytes <= 2000000){
						$kb = $bytes / 1024;
						$b2 = $bytes % 1024;
						$mb = $kb / 1024;
						$kb2 = $kb % 1024;
						$gb = $mb / 1024;
						$mb2 = $mb % 1024;
						printf ("<p>$bytes bytes son: $gb GigaBytes, $mb2 MegaBytes, $kb2 KiloBytes y $b2 Bytes.</p>");
					}else{
						printf ("<p>Debe introducir un número entre 0 y 2000000</p>");
					}
				}else{
					printf ("<p>Debe introducir un número</p>");			
				}		
			}
		?>
	</body>
</html>