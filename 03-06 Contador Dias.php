<!DOCTYPE html>
<html>
	<head>
		  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		  <title>Contador de días (Formulario). Repaso 1. Ejercicios. PHP. Bartolomé Sintes Marco</title>
	</head>
	
	<body>
		<h1>Contador de días (Formulario)</h1>		
		<form action="03-06 Contador Dias.php" method="get">
			  <fieldset>
				    <legend>Formulario</legend>
				    <p>Escriba un número de semanas (0 &lt; semanas &le; 20) y mostraré un calendario.</p>				
				    <table cellspacing="5" class="borde">
					      <tbody>
						        <tr>
							          <td>
							          		<strong>Número de semanas:</strong>
							          </td>
							          <td>
							          		<input type="text" name="numero" size="4" maxlength="2" />
							          </td>
						        </tr>
					      </tbody>
				    </table>				
				    <p class="der">
					    <input type="submit" value="Enviar" /> 
					    <input type="reset" value="Borrar" name="Reset" />
					</p>
			  </fieldset>
		</form>
		<?php
			global $dias;
			$dias = array('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo');
			function muestra_tabla($num){				
				print "<form action='dias.php' method='get'>
						<table style='text-align: center;'>
							<thead>
								<tr>";
				foreach ($dias as $dia) {
					print "<th>$dia</th>";
				}
				print "</tr></thead>"; //fin de cabecera
				for ($x = 0; $x < $num; $x ++){
					print "<tr>";
					for ($i = 0; $i < 7; $i ++){
						print "<td><input type='checkbox' name='dias[$x][$i]'/></td>";	 
						//con echo "<input type=\"checkbox\" name..."					
					}
					print "</tr>";
				}
				print "</table>
						<br />
						<input type='submit' value='Enviar' />
					</form>";
			}			
			function muestra_dias($datos){
				
				foreach ($datos as $n => $valor) {
					echo "La semana " . ($n + 1) . " se han seleccionado " . count($valor) . " dias: ";
					foreach ($valor as $dia => $x){
						print $dias[$dia]. " ";
					}
					print "<br />";
				}
			}
		
			if (isset($_GET['numero'])) {
				$numero = $_GET['numero'];
				if(is_numeric($numero)){
					if ($numero  > 0 && $numero < 21){
						muestra_tabla($numero);
					}else{
						printf ("<p>Debe introducir un numero entre 0 y 20</p>");
					}
				}
			}
			if (isset($_GET['dias'])){
				$datos = $_GET['dias'];
				muestra_dias($datos);
			}
		?>
	</body>
</html>