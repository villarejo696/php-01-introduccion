<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
  		<title>Comprobador de múltiplos (Formulario). if ... elseif ... else ... Ejercicios. PHP. Bartolomé Sintes Marco</title>
	</head>
	<body>
		<h1>Comprobador de múltiplos (Formulario)</h1>
		<form action="02-02 Comprobador Multiplos.php" method="get">
			  <fieldset>
				    <legend>Formulario</legend>
				    <p>Escriba dos números enteros (0 &lt; números &lt; 1.000) para comprobar si uno es múltiplo del otro o no.</p>
				    <table cellspacing="5" class="borde">
					      <tbody>
						        <tr>
							          <td>
							          		<strong>Un número:</strong>
							          </td>
							          <td>
							          		<input type="text" name="numero1" size="5" maxlength="5" />
							          </td>
						        </tr>
						        <tr>
							          <td>
							          		<strong>Otro número:</strong>
							          </td>
							          <td>
							          		<input type="text" name="numero2" size="5" maxlength="5" />
							          </td>
						        </tr>
					      </tbody>
				    </table>
				    <p class="der">
					    <input type="submit" value="Calcular" /> 
					    <input type="reset" value="Borrar" name="Reset" />
				    </p>
			  </fieldset>
		</form>	
		<?php
			if (isset($_GET['numero1'], $_GET['numero2'])) {
				$numero1 = $_GET['numero1'] ;
				$numero2 = $_GET['numero2'] ;
				$resultado = 0;
				
				if(is_numeric($numero1) && is_numeric($numero2)){
						if ($numero1 > 0 && $numero1 < 1000 && $numero2 > 0 && $numero2 < 1000) {
						if ($numero1 % $numero2 === 0){
							printf ("<p> $numero1 es multiplo de $numero2</p>");
						}else{
							printf ("<p> $numero1 no es multiplo de $numero2</p>");	
						}	
					} else {
						printf ("<p>Debe introducir los datos correctos	(0 < numero1 < 1.000; 0 < numero2 < 1.000)</p>");
					}
				}else{
						printf ("<p>Debe introducir un numero</p>");			
				}		
			}
		?>
	</body>
</html>