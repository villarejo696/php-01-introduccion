<!DOCTYPE html>
<html>
	<head>
		  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		  <title>Ecuación de segundo grado (Formulario). Repaso (1) Ejercicios. PHP. Bartolomé Sintes Marco</title>
	</head>	
	<body>
		<h1>Ecuación de segundo grado (Formulario)</h1>
		<form action="03-04 Calculadora Ecuaciones Grado 2.php" method="get">
			  <fieldset>
				    <legend>Formulario</legend>
				    <p>Dado la ecuación de segundo grado <span style="font-size: 200%">a.x<sup>2</sup> + b.x + c = 0</span>, escriba los valores de los tres coeficientes y resolveré la ecuación:</p>
				    <table cellspacing="5" class="borde">
					      <tbody>
						        <tr>
          							<td>
          								<strong>a:</strong>
          							</td>
          							<td>
          								<input type="text" name="a" size="7" maxlength="7" />
          							</td>
						        </tr>
						        <tr>
							        <td>
							        	<strong>b:</strong>
							        </td>
							        <td>
							        	<input type="text" name="b" size="7" maxlength="7" />
							        </td>
						        </tr>
						        <tr>
							        <td>
							        	<strong>c:</strong>
							        </td>
							        <td>
							        	<input type="text" name="c" size="7" maxlength="7" />
							        </td>
						        </tr>
					      </tbody>
				    </table>
			    	<p class="der">
			    		<input type="submit" value="Calcular" /> 
			    		<input type="reset" value="Borrar" name="Reset" />
			  		</p>
			  </fieldset>
		</form>
		<?php
			if (isset($_GET['a'], $_GET['b'], $_GET['c'])) {
				$a = $_GET['a'];
				$b = $_GET['b'];
				$c = $_GET['c'];
				if(is_numeric($a) && is_numeric($b) && is_numeric($c)){
					$bac = (b * b) + (4 * a * c);
					if ($bac > 0){
						$res1 = (- $b + sqrt($bac)) / (2 * $a);
						$res2 = (- $b - sqrt($bac)) / (2 * $a);
						printf ("<p>Los resulados de a = $a, b = $b y c = $c son: $res1 y $res2</p>");	
					}elseif ($bac == 0){
						$res = - $b / (2 * $a);
						printf ("<p>El resultado de a = $a, b = $b y c = $c es: $res</p>");	
					}else{
						printf ("<p>La ecuación no tiene resultados</p>");	
					}
				}else{
					printf ("<p>Debe introducir numeros</p>");			
				}		
			}
		?>
	</body>
</html>