<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Convertidor de pies y pulgadas a centímetros (Formulario). Operaciones aritméticas. Ejercicios. PHP. Bartolomé Sintes Marco</title>
	</head>
	<body>
		<h1>Convertidor de pies y pulgadas a centímetros (Formulario)</h1>
		<form action="01-02 Conversor Pies Pulgadas a Cm.php" method="get">
			<fieldset>
				<legend>Formulario</legend>
				<p>Escriba un número de pies y pulgadas para convertir a centímetros.</p>
				<table cellspacing="5">
					<tbody>
						<tr>
							<td>
								<strong>Pies:</strong>
							</td>
							<td>
								<input type="text" name="pies" size="5" maxlength="5" />
							</td>
						</tr>
						<tr>
							<td>
								<strong>Pulgadas:</strong>
							</td>
							<td>
								<input type="text" name="pulgadas" size="5" maxlength="5" />
							</td>
						</tr>
					</tbody>
				</table>
				<p class="der">
					<input type="submit" value="Convertir" /> 
					<input type="reset" value="Borrar" name="Reset" />
				</p>
			</fieldset>
		</form>
		<?php
			if (isset($_GET['pies'], $_GET['pulgadas'])){
				$pies = $_GET['pies'];
				$pulgadas = $_GET['pulgadas'];
				
				echo "<p> $pies pies y $pulgadas pulgadas son:";
				
				if ($pies != null){
					$pulgadas = $pulgadas + ($pies * 12);
				}
				$resultado = $pulgadas * 2.54;
			
				echo " $resultado centimetros</p>";
			}
		?>
	</body>
</html>
