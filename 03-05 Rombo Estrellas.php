<!DOCTYPE html>
<html>
	<head>
		  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		  <title>Rombo de estrellas (Formulario). Repaso (1). Ejercicios. PHP. Bartolomé Sintes Marco</title>
	</head>	
	<body>
		<h1>Rombo de estrellas (Formulario)</h1>
		<form action="03-05 Rombo Estrellas.php" method="get">
			  <fieldset>
				    <legend>Formulario</legend>
				    <p>Escriba el alto (0 &lt; alto &le; 30) y mostraré un rombo de estrellas de ese tamaño.</p>
				    <table cellspacing="5" class="borde">
					      <tbody>
						        <tr>
          							<td>
          								<strong>Alto:</strong>
          							</td>
          							<td>
          								<input type="text" name="alto" size="3" maxlength="3" />
          							</td>
						        </tr>
					      </tbody>
				    </table>
			    	<p class="der">
			    		<input type="submit" value="Dibujar" /> 
			    		<input type="reset" value="Borrar" name="Reset" />
			  		</p>
			  </fieldset>
		</form>
		<?php
			if (isset($_GET['alto'])) {
				$alto = $_GET['alto'];
				if(is_numeric($alto)){
					if ($alto > 0 && $alto <= 30){
						printf ("<p>");
						$x = 0;
						foreach ($num1 as $alto) {
							foreach ($num2 as $alto) {
								printf("*");
							}
							printf("<br />");
							$x += 1;
							foreach ($num3 as $x){
								printf(" ");
							}
						}
						printf("</p>");
					}else{
						printf ("<p>Debe introducir un numero entre 0 y 30</p>");	
					}
				}else{
					printf ("<p>Debe introducir un numero</p>");			
				}		
			}
		?>
	</body>
</html>