<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Indice de masa corporal (Formulario). Operaciones aritméticas. Ejercicios. PHP. Bartolomé Sintes Marco</title>
	</head>
	<body>
		<h1>Indice de masa corporal (Formulario)</h1>
		<form action="01-01 masacorporal.php" method="get">
			<fieldset>
				<legend>Formulario</legend>
				<p>Escriba su peso en kilogramos y su altura en centímetros para calcular su índice de masa corporal.</p>
				<table cellspacing="5" class="borde">
					<tbody>
						<tr>
							<td>
								<strong>Peso:</strong>
							</td>
							<td>
								<input type="text" name="peso" size="5" maxlength="5" /> kg
							</td>
						</tr>
						<tr>
							<td>
								<strong>Altura:</strong>
							</td>
							<td>
								<input type="text" name="altura" size="5" maxlength="5" /> cm
							</td>
						</tr>
					</tbody>
				</table>
				<p class="der">
					<input type="submit" value="Calcular" /> 
					<input type="reset" value="Borrar" name="Reset" />
				</p>
			</fieldset> 
		</form>
		<?php
			if (isset($_GET['peso'], $_GET['altura'])){
				$peso = $_GET['peso'];
				$altura = $_GET['altura'];
				$masa_corporal = $peso / pow($altura/100, 2); 
				return $masa_corporal;
				$masa_corporal = imc($peso = $_GET['peso'],	$altura = $_GET['altura']);
				printf ("<p> Con el peso $peso y la altura $altura , tenemos una masa corporal de %.2f</p>", $masa_corporal);
			}
		?>
	</body>
</html>
