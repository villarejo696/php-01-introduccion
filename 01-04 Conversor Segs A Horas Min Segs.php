<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Convertidor de segundos a minutos y segundos (Formulario). Operaciones aritméticas. Ejercicios. PHP. Bartolomé Sintes Marco</title>
	</head>
	<body>
		<h1>Convertidor de segundos a minutos y segundos (Formulario)</h1>
		<form action="01-04 Conversor Segs a HorasMinSegs.php" method="get">
			<fieldset>
    			<legend>Formulario</legend>
    			<p>Escriba un número de segundos para convertir a minutos y segundos.</p>
    			<table cellspacing="5" class="borde">
      				<tbody>
        				<tr>
          					<td>
          						<strong>Segundos:</strong>
          					</td>
          					<td>
          						<input type="text" name="segundos" size="8" maxlength="8" />
          					</td>
        				</tr>
      				</tbody>
    			</table>
    			<p class="der">
    				<input type="submit" value="Convertir" /> 
    				<input type="reset" value="Borrar" name="Reset" />
    			</p>
  			</fieldset>
		</form>
		<?php
			if (isset($_GET['segundos'])){
				$segundos = $_GET['segundos'];
						
				echo "<p> $segundos segundos son:";
				
				$minutos = $segundos / 60 ;
				$segundos = $segundos % 60;
				$horas =$minutos / 60;
				$minutos = $minutos % 60;
			
				echo " $horas h $minutos min $segundos segundos</p>";
			}
		?>
	</body>
</html>
