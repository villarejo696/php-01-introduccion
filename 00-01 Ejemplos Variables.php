<h1>Eejercicio con variables</h1>
<?php
	$precio = 50; //precio  --> 50 euros
	/*
	 * con comillas simples no interpreta las cadenas de caracteres.
	 * con comillas dobles podremos poner variables sin problema.
	 * 
	 */
	echo '<p>El precio es $precio</p>';
	echo "<p>El precio es $precio</p>";
	
	echo "\n";
	//funcionan todas bien
	echo '<p>El precio es'.$precio.'</p>'; 
	echo '<p>El precio es', $precio,'</p>';
	echo "<p>El precio es", $precio,"</p>";


?>
<h1>Variables globales</h1>
<?php
	
	echo "<p>archivo que se esta ejecutando: ", $_SERVER['PHP_SELF'], "</p>" ;
	echo "<p>El servidor esta en: ", $_SERVER['SERVER_ADDR'],"</p>" ;
	echo "<p>Me conecto desde: ", $_SERVER['REMOTE_ADDR'],"</p>" ;
	echo "<p>archivo que se esta ejecutando", $_SERVER['PHP_SELF'],"</p>" ;
	
?>