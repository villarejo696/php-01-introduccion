<!DOCTYPE html>
<html>
	<head>
		  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		  <title>Convertidor de divisas (Formulario). Repaso 1. Ejercicios. PHP. Bartolomé Sintes Marco</title>
	</head>	
	<body>
		<h1>Convertidor de divisas (Formulario)</h1>
		<form action="03-01 Conversor De Divisas A Euros.php" method="get">
			<fieldset>
				<legend>Formulario</legend>
				<p>Escriba la cantidad de dinero (0 &lt; valores &lt; 1.000.000) y elija la divisa:</p>
				<p>Convertir en euros
					<input type="text" name="cantidad" size="6" maxlength="6" />
					<select name="origen">
						<option selected="selected" value="USD">Dólares USA</option>
						<option value="GBP">Libras esterlinas</option>
						<option value="JPY">Yenes</option>
						<option value="ESP">Pesetas</option>
					</select>
				</p>
				<p><strong>Nota</strong>: La cotización de las monedas no está actualizada. Los valores utilizados son:<br /> 1 euro = 1,31481 dólares USA = 0,89807 libras esterlinas = 132,113 yenes japoneses = 166,386 antiguas pesetas españolas</p>
				<p class="der">
					<input type="submit" value="Convertir" /> 
					<input type="reset" value="Borrar" name="Reset" />
				</p>
			</fieldset>
		</form>
		<?php
			if (isset($_GET['cantidad'], $_GET['origen'])) {
				$cant = $_GET['cantidad'];
				$ori = $_GET['origen'];
				if(is_numeric($cant)){
					if ($cant > 0 && $ $cant < 1000000){
						$res = 0;
						switch ($cant) {
							case 'USD':
								$res = $cant / 1.31481;
								break;
							case 'GBP':
								$res = $cant / 0.89807;
								break;
							case 'JPY':
								$res = $cant / 132.113;
								break;
							case 'ESP':
								$res = $cant / 166.386;
								break;
						}
						printf ("<p>$cant $ori son: $res €</p>");
					}else{
						printf ("<p>Debe introducir un numero entre 0 y 1.000.000</p>");
					}		
				}else{
					printf ("<p>Debe introducir un numero</p>");			
				}		
			}
		?>
	</body>
</html>