<!DOCTYPE html>
<html>
	<head>
		  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		  <title>Convertidor de temperaturas Celsius / Fahrenheit (Formulario). if ... elseif ... else ... Ejercicios. PHP. Bartolomé Sintes Marco</title>
	</head>
	
	<body>
		<h1>Convertidor de temperaturas Celsius / Fahrenheit (Formulario)</h1>
		<form action="02-06 Conversor Teperatura Celsius Fahrenheit.php" method="get">
			<fieldset>
				<legend>Formulario</legend>
				<p>Escriba una temperatura en grados Celsius o Fahrenheit (-273.15 &le; Celsius &lt; 10.000; -459.67 &le; Fahrenheit &lt; 10.000) para convertila a la otra unidad (Fahrenheit o Celsius).</p>
				<table cellspacing="5" class="borde">
					<tbody>
						<tr>
							<td>
								<strong>Temperatura:</strong>
							</td>
							<td>
								<input type="text" name="temperatura" size="7" maxlength="7" /> 
								<select name="unidad">
									<option value="c" selected="selected">Celsius</option>
									<option value="f">Fahrenheit</option>
								</select>
							</td>
						</tr>
					</tbody>
				</table>
				<p class="der">
					<input type="submit" value="Convertir" /> 
					<input type="reset" value="Borrar" name="Reset" />
				</p>
			</fieldset>
		</form>
		<?php
			if (isset($_GET['temperatura'], $_GET['unidad'])) {
				$temp = $_GET['temperatura'];
				$unidad = $_GET['unidad'];
				
				if(is_numeric($temp)){
					if ($unidad == "c") {
							if ($temp > -273.15 && $temp < 10000){
								$res = ($temp * 1.8) + 32;
								printf ("<p>$temp grados Celsius son: $res grados Fahrenheit</p>");
							}else{
								printf ("<p>Debe introducir una temperatura correcta</p>");
							}
					} else {
						if ($temp > -495.67 && $temp < 10000){
								$res = ($temp - 32) * 1.8;
								printf ("<p>$temp grados Fahrenheit son: $res grados Celsius</p>");
							}else{
								printf ("<p>Debe introducir una temperatura correcta</p>");
							}
					}
				}else{
						printf ("<p>Debe introducir un número</p>");			
				}		
			}
		?>
	</body>
</html>