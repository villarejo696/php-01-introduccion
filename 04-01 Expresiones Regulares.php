<!DOCTYPE html>
<html>
	<head>
		  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		  <title>Validación de entrada de texto 1 (Formulario). Expresiones regulares. Ejercicios. PHP. Bartolomé Sintes Marco</title>
	</head>
	
	<body>
		<h1>Validación de entrada de texto 1 (Formulario)</h1>		
		<form action="04-01 Expresiones Regulares.php" method="get">
			  <fieldset>
				    <legend>Formulario</legend>
				    <p>
				    	Escriba algo:
				    	<input type="text" name="texto" size="80" maxlength="80" />
				    </p>
				    <p class="der">
				    	<input type="submit" value="Enviar" /> 
				    	<input type="reset" value="Borrar" name="Reset" />
				    </p>
			  </fieldset>
		</form>
		<?php
			if (isset($_GET['texto'])) {
				$cadena = $_GET['texto'];
				
				$patron2 = "/^[[:alpha:]]+$/"; // Una palabra (sólo letras)
				$patron3 = "/^[[:alpha:]]+ +[[:alpha:]]+$/"; // Dos palabras (separadas por uno o más espaccios)
				$patron4 = "/^[a-z]+$/i"; // Una palabra sólo con letras inglesas
				$patron5 = "/^a*e*i*o*u*$/"; // Cadena de vocales ordenadas alfabéticamente (se pueden repetir)
				$patron6 = "/^[0-9]+$/"; // Número entero
				$patron7 = "/^[0-9]*[02468]$/"; // Número entero par
				$patron8 = "/^[69][0-9]{8}$/"; // Número de teléfono (empieza por 6 o 9)
				$patron9 = "/^[0-9]{1,8}[A-Z]?$/"; // DNI (8 números con letra en mayúscula)
				$patron10 = "/^[0-4][0-9]{4}$/"; // Código Postal
				
				if ($cadena == "") {
    				print "<p>La cadena $cadena está vacía.</p>\n";
				}
				if (preg_match($patron2, $cadena)) {
    				print "<p>La cadena $cadena es una sola palabra.</p>\n";
				}
				if (preg_match($patron3, $cadena)) {
    				print "<p>La cadena $cadena son dos palabras separadas por uno o más espacios.</p>\n";
				}
				if (preg_match($patron4, $cadena)) {
    				print "<p>La cadena $cadena es una palabra sólo con letras inglesas.</p>\n";
				}
				if (preg_match($patron5, $cadena)) {
    				print "<p>La cadena $cadena es una cadena de vocales ordenadas alfabéticamente.</p>\n";
				}
				if (preg_match($patron6, $cadena)) {
    				print "<p>La cadena $cadena es un número entero.</p>\n";
				}
				if (preg_match($patron7, $cadena)) {
    				print "<p>La cadena $cadena es un número entero par.</p>\n";
				}
				if (preg_match($patron8, $cadena)) {
    				print "<p>La cadena $cadena es un número de teléfono.</p>\n";
				}
				if (preg_match($patron9, $cadena)) {
    				print "<p>La cadena $cadena es un número de DNI.</p>\n";
				}
				if (preg_match($patron10, $cadena)) {
    				print "<p>La cadena $cadena es un Código Postal.</p>\n";
				}
			}
		?>
	</body>
</html>