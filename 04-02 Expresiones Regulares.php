<!DOCTYPE html>
<html>
	<head>
		  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		  <title>Validación de entrada de texto 2 (Formulario). Expresiones regulares. Ejercicios. PHP. Bartolomé Sintes Marco</title>
	</head>
	
	<body>
		<h1>Validación de entrada de texto 2 (Formulario)</h1>		
		<form action="04-02 Expresiones Regulares.php" method="get">
			  <fieldset>
				    <legend>Formulario</legend>
				    <p>
				    	Escriba algo:
				    	<input type="text" name="texto" size="80" maxlength="80" />
				    </p>
				    <p class="der">
				    	<input type="submit" value="Enviar" /> 
				    	<input type="reset" value="Borrar" name="Reset" />
				    </p>
			  </fieldset>
		</form>
		<?php
			if (isset($_GET['texto'])) {
				$cadena = $_GET['texto'];
				$patron1 = "/^[[:alpha:]]( +[[:alpha:]])*$/"; // Una o más letras separadas por espacios
				$patron2 = "/^[[:alpha:]]( +[[:alpha:]])+$/"; // Dos o más letras separadas por espacios
				$patron3 = "/^[a-z]+( +[a-z]+)*$/i"; // Una o más palabras separadas por uno o más espacios, sólo letras inglesas
				$patron4 = "/^[[:upper:]]+$/"; // Una palabla en mayúscula
				$patron5 = "/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/"; // Feha dd/mm/aaaa
				$patron6 = "/^[[:digit:]]+([\.,][[:digit:]]{1,2})?$/"; // Un número sin signo y con dos decimales máximo utilizando . o , si los hay
				$patron7 = "/^[+-][[:digit:]]+([\.,][[:digit:]]+)?$/"; // Un número con signo y con decimales utilizando . o , si los hay
				$patron8 = "/^[[:alnum:]\+\.\*_-]{6,}$/"; // Contraseña: 6 carácteres, con letras, números y/o *+.;-_
				
				if (preg_match($patron1, $cadena)) {
    				print "<p>La cadena $cadena es una o más letras separadas por espacios.</p>\n";
				}
				if (preg_match($patron2, $cadena)) {
    				print "<p>La cadena $cadena son dos o más letras separadas por espacios.</p>\n";
				}
				if (preg_match($patron3, $cadena)) {
    				print "<p>La cadena $cadena son una o más palabras separadas por uno o más espacios, sólo letras inglesas.</p>\n";
				}
				if (preg_match($patron4, $cadena)) {
    				print "<p>La cadena $cadena es una palabla en mayúscula.</p>\n";
				}
				if (preg_match($patron5, $cadena)) {
    				print "<p>La cadena $cadena es una feha dd/mm/aaaa.</p>\n";
				}
				if (preg_match($patron6, $cadena)) {
    				print "<p>La cadena $cadena es un número sin signo y con dos decimales máximo utilizando . o , si los hay.</p>\n";
				}
				if (preg_match($patron7, $cadena)) {
    				print "<p>La cadena $cadena es un número con signo y con decimales utilizando . o , si los hay.</p>\n";
				}
				if (preg_match($patron8, $cadena)) {
    				print "<p>La cadena $cadena es una contraseña: 6 carácteres, con letras, números y/o *+.;-_.</p>\n";
				}
			}
		?>
	</body>
</html>