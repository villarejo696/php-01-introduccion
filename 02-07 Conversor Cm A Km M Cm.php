<!DOCTYPE html>
<html>
	<head>
		  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		  <title>Convertidor de centímetros a kilómetros, metros y centímetros (Formulario) if ... elseif ... else ... Ejercicios. PHP. Bartolomé Sintes Marco</title>
	</head>
	
	<body>
		<h1>Convertidor de centímetros a kilómetros, metros y centímetros (Formulario)</h1>
		<form action="02-07 Conversor Cm A Km M Cm.php" method="get">
			<fieldset>
				<legend>Formulario</legend>
				<p>Escriba una distancia en centímetros (0 &le; distancia &lt; 1.000.000.000) para convertirla a kilómetros, metros y centímetros.</p>
				<table cellspacing="5" class="borde">
					<tbody>
						<tr>
							<td>
								<strong>Distancia:</strong>
							</td>
							<td>
								<input type="text" name="distancia" size="9" maxlength="9" /> cm
							</td>
						</tr>
					</tbody>
				</table>
				<p class="der">
					<input type="submit" value="Convertir" /> 
					<input type="reset" value="Borrar" name="Reset" />
				</p>
			</fieldset>
		</form>
		<?php
			if (isset($_GET['distancia'])) {
				$cm = $_GET['distancia'];
				
				if(is_integer($cm)){
					if ($cm > -1 && $cm < 1000000000 ) {
						$m = $cm / 1000;
						$cm2 = $cm % 1000;
						$km = $m / 1000;
						$m2 = $m % 1000;
						printf ("<p>$cm centímetros son: $km Kilómetros, $m2 metros y $cm2 centímetros.</p>");
					} else {
						printf ("<p>Debe introducir los datos correctos, un número entero entre 0 y 1.000.000.000</p>");
					}
				}else{
						printf ("<p>Debe introducir un numero</p>");			
				}		
			}
		?>
	</body>
</html>