<!DOCTYPE html>
<html>
	<head>
		  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		  <title>Comparador de tres números (Formulario). if ... elseif ... else ... Ejercicios. PHP. Bartolomé Sintes Marco</title>
	</head>
	
	<body>
		<h1>Comparador de tres números (Formulario)</h1>		
		<form action="02-03 Comparador 3 numeros.php" method="get">
			  <fieldset>
				    <legend>Formulario</legend>
				    <p>Escriba tres números (-1.000 &lt; números &lt; 1.000) para comprobar si hay números iguales.</p>				
				    <table cellspacing="5" class="borde">
					      <tbody>
						        <tr>
							          <td>
							          		<strong>Primer número:</strong>
							          </td>
							          <td>
							          		<input type="text" name="numero1" size="6" maxlength="6" />
							          </td>
						        </tr>
						        <tr>
							          <td>
							          		<strong>Segundo número:</strong>
							          </td>
							          <td>
							          		<input type="text" name="numero2" size="6" maxlength="6" />							          	
							          </td>
						        </tr>
						        <tr>
							          <td>
							          		<strong>Tercer número:</strong>
							          </td>
							          <td>
							          		<input type="text" name="numero3" size="6" maxlength="6" />
							          </td>
						        </tr>
					      </tbody>
				    </table>		
				    <p class="der">
					    <input type="submit" value="Comparar" /> 
					    <input type="reset" value="Borrar" name="Reset" />
					</p>
			  </fieldset>
		</form>
		<?php
			if (isset($_GET['numero1'], $_GET['numero2'], $_GET['numero3'])) {
				$numero1 = $_GET['numero1'];
				$numero2 = $_GET['numero2'];
				$numero3 = $_GET['numero3'];
				$resultado = 0;
				
				if(is_numeric($numero1) && is_numeric($numero2) && is_numeric($numero3)){
					if ($numero1 > -1000 && $numero1 < 1000 && $numero2 > -1000 && $numero2 < 1000 && $numero3 > -1000 && $numero3 < 1000) {
						if ($numero1 == $numero2 && $numero1 == $numero3){
							printf ("<p> num1 $numero1 num2 $numero2 y num3 $numero3 son iguales</p>");
						}elseif ($numero1 == $numero2){
							printf ("<p> num1 $numero1 y num2 $numero2 son iguales</p>");	
						}elseif ($numero1 == $numero3){
							printf ("<p> num1 $numero1 y num3 $numero3 son iguales</p>");	
						}elseif ($numero2 == $numero3){
							printf ("<p> num2 $numero2 y num3 $numero3 son iguales</p>");	
						}else {
							printf ("<p> Los numeros son distintos</p>");
						}	
					} else {
						printf ("<p>Debe introducir los datos correctos	(0 <= dividendo < 1.000; 0 < divisor < 1.000)</p>");
					}
				}else{
						printf ("<p>Debe introducir un numero</p>");			
				}		
			}
		?>
	</body>
</html>