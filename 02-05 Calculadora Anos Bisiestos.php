<!DOCTYPE html>
<html>
	<head>
		  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		  <title>Calculadora de años bisiestos (Formulario). if ... elseif ... else ... Ejercicios. PHP. Bartolomé Sintes Marco</title>
	</head>
	
	<body>
		<h1>Calculadora de años bisiestos (Formulario)</h1>		
		<form action="02-05 Calculadora Anos Bisiestos.php" method="get">
			<fieldset>
				<legend>Formulario</legend>
				<p>Escriba un año (0 &le; año &lt; 10.000) para comprobar si es bisiesto o no.</p>
				<table cellspacing="5" class="borde">
					<tbody>
					<tr>
						<td>
							<strong>Año:</strong>
						</td>
						<td>
							<input type="text" name="anyo" size="5" maxlength="5" />
						</td>
					</tr>
				</tbody>
				</table>
				<p class="der">
					<input type="submit" value="Calcular" /> 
					<input type="reset" value="Borrar" name="Reset" />
				</p>
			</fieldset>
		</form>
		<?php
			if (isset($_GET['anyo'])) {
				$anyo = $_GET['anyo'];
				
				if(is_numeric($anyo)){
					if ($anyo > 0 && $anyo < 10000) {
							if ($anyo % 4 == 0){
								printf ("<p>El año introducido $anyo es bisiesto</p>");									
								
							}else{
								printf ("<p>El año introducido $anyo no es bisiesto</p>");
							}
					} else {
						printf ("<p>Debe introducir los datos correctos, un año positivo e inferior a 10000</p>");
					}
				}else{
						printf ("<p>Debe introducir un numero entero</p>");			
				}		
			}
		?>
	</body>
</html>